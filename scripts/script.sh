#!/bin/bash

ORDERER_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cityhub.org/orderers/orderer1.cityhub.org/msp/tlscacerts/tlsca.cityhub.org-cert.pem
PEER1_PNU_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/pnu.cityhub.org/peers/peer2.pnu.cityhub.org/tls/ca.crt
PEER1_N2M_CA=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/n2m.cityhub.org/peers/peer2.n2m.cityhub.org/tls/ca.crt

CHANNEL_NAME="$1"
DELAY="$2"
COUNTER=1
MAX_RETRY=10
LANGUAGE="golang"
CC_SRC_PATH="cityhub.org/token"

declare -a ORGS=("pnu" "n2m")

echo "Channel name : "$CHANNEL_NAME

# verify the result of the end-to-end test
verifyResult() {
    if [ $1 -ne 0 ]; then
        echo "!!!!!!!!!!!!!!! "$2" !!!!!!!!!!!!!!!!"
        echo "========= ERROR !!! FAILED init network ==========="
        echo
        exit 1
    fi
}

setGlobals() {
    PEER=$1
    ORG=$2
    if [ $ORG = "pnu" ]; then
        CORE_PEER_LOCALMSPID="PNUMSP"
        CORE_PEER_TLS_ROOTCERT_FILE=$PEER1_PNU_CA
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/pnu.cityhub.org/users/Admin@pnu.cityhub.org/msp
        if [ $PEER -eq 1 ]; then
            CORE_PEER_ADDRESS=peer1.pnu.cityhub.org:7051
        else
            CORE_PEER_ADDRESS=peer2.pnu.cityhub.org:8051
        fi
    elif [ $ORG = "n2m" ]; then
        CORE_PEER_LOCALMSPID="N2MMSP"
        CORE_PEER_TLS_ROOTCERT_FILE=$PEER1_N2M_CA
        CORE_PEER_MSPCONFIGPATH=/opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/peerOrganizations/n2m.cityhub.org/users/Admin@n2m.cityhub.org/msp
        if [ $PEER -eq 1 ]; then
            CORE_PEER_ADDRESS=peer1.n2m.cityhub.org:9051
        else
            CORE_PEER_ADDRESS=peer2.n2m.cityhub.org:10051
        fi
    else
        echo "================== ERROR !!! ORG Unknown =================="
    fi
}

createChannel() {
    setGlobals 1 "pnu"

    peer channel create -o orderer1.cityhub.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/channel.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA --outputBlock ./channel-artifacts/$CHANNEL_NAME.block

    echo "===================== Channel '$CHANNEL_NAME' created ===================== "
    echo
}

joinChannel() {
    for org in "${ORGS[@]}"; do
        for peer in 1 2; do
            joinChannelWithRetry $peer $org
            echo "===================== peer${peer}.${org} joined channel '$CHANNEL_NAME' ===================== "
            sleep $DELAY
            echo
        done
    done
}

## Sometimes Join takes time hence RETRY at least 5 times
joinChannelWithRetry() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG

    set -x
    peer channel join -b ./channel-artifacts/$CHANNEL_NAME.block >&log.txt
    res=$?
    set +x
    cat log.txt
    if [ $res -ne 0 -a $COUNTER -lt $MAX_RETRY ]; then
        COUNTER=$(expr $COUNTER + 1)
        echo "${PEER}.${ORG} failed to join the channel, Retry after $DELAY seconds"
        sleep $DELAY
        joinChannelWithRetry $PEER $ORG
    else
        COUNTER=1
    fi
    verifyResult $res "After $MAX_RETRY attempts, peer${PEER}.${ORG} has failed to join channel '$CHANNEL_NAME' "
}

updateAnchorPeers() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG

    set -x
    peer channel update -o orderer1.cityhub.org:7050 -c $CHANNEL_NAME -f ./channel-artifacts/${CORE_PEER_LOCALMSPID}anchors.tx --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA >&log.txt
    res=$?
    set +x

    cat log.txt
    verifyResult $res "Anchor peer update failed"
    echo "===================== Anchor peers updated for org '$CORE_PEER_LOCALMSPID' on channel '$CHANNEL_NAME' ===================== "
    sleep $DELAY
    echo
}

buildChaincode() {
    mkdir -p $GOPATH/src/cityhub.org
    cp -r ./chaincode/token "$_" && cd $GOPATH/src/cityhub.org/token && GOCACHE=on GO111MODULE=on go mod vendor
}

installChaincode() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG
    VERSION=${3:-1.0}
    set -x
    peer chaincode install -n tokencc -v ${VERSION} -l ${LANGUAGE} -p ${CC_SRC_PATH} >&log.txt
    # peer chaincode install -n tokencc -v 1.0 -l golang -p 'cityhub.org/token' >&log.txt
    res=$?
    set +x
    cat log.txt
    verifyResult $res "Chaincode installation on peer${PEER}.${ORG} has failed"
    echo "===================== Chaincode is installed on peer${PEER}.${ORG} ===================== "
    echo

    

    
}

instantiateChaincode() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG
    VERSION=${3:-1.0}

    set -x
    # TODO: Add policy option 
    peer chaincode instantiate -o orderer1.cityhub.org:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME -n tokencc -l ${LANGUAGE} -v ${VERSION} -c '{"Args":["PNUT","PusanNationalUniversityToken","10000000"]}' -P 'OR ("PNUMSP.member","N2MMSP.member")' >&log.txt
    # peer chaincode instantiate -n tokencc -v ${VERSION} -c '{"Args":["CMT","CityhubMarketplaceToken","100000"]}' --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA -C $CHANNEL_NAME  -l ${LANGUAGE}  
    # peer chaincode instantiate -o orderer1.cityhub.org:7050 --tls true --cafile /opt/gopath/src/github.com/hyperledger/fabric/peer/crypto/ordererOrganizations/cityhub.org/orderers/orderer1.cityhub.org/msp/tlscacerts/tlsca.cityhub.org-cert.pem -C marketplace-channel -n tokencc -l golang -v 1.0 -c '{"Args":["SWT","ShinWookToken","10000"]}' -P 'OR ("PNUMSP.member","N2MMSP.member")' >&log.txt
    res=$?
    set +x

    echo "Wait 10 seconds for chaincode to be instantiated"
    sleep 10

    verifyResult $res "Chaincode instantiation on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' failed"

    echo "===================== Chaincode is instantiated on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' ===================== "
    echo
    
}

testChaincode() {
    PEER=$1
    ORG=$2
    setGlobals $PEER $ORG
    VERSION=${3:-1.0}


    echo "===================== Chaincode is invoked on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' ===================== "
    echo    

    set -x
    peer chaincode invoke -n testcc -c '{"Args":["set", "a", "1000"]}' -C $CHANNEL_NAME -o orderer1.cityhub.org:7050 --tls $CORE_PEER_TLS_ENABLED --cafile $ORDERER_CA
    res=$?
    set +x

    verifyResult $res "Chaincode Invoke on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' failed"
    
    sleep 5

    echo "===================== Chaincode is queried on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' ===================== "
    echo   

    set -x
    peer chaincode query -n testcc -c '{"Args":["query","a"]}' -C $CHANNEL_NAME
    res=$?
    set +x

    verifyResult $res "Chaincode Qurey on peer${PEER}.${ORG} on channel '$CHANNEL_NAME' failed"
    
}


## Create channel
echo "Creating channel..."
createChannel

## Join all the peers to the channel
echo "Having all peers join the channel..."
joinChannel

## Set the anchor peers for each org in the channel
echo "Updating anchor peers for pnu..."
updateAnchorPeers 1 pnu
echo "Updating anchor peers for n2m..."
updateAnchorPeers 1 n2m

echo "Building cityhub token chaincode..."
buildChaincode

## Install chaincode on peer1.pnu.cityhub.org and peer1.n2m.cityhub.org
echo "Installing chaincode on peer1.pnu.cityhub.org"
installChaincode 1 pnu

# echo "Installing chaincode on peer1.pnu.cityhub.org"
# installChaincode 1 n2m

# Instantiate chaincode on peer1.pnu
echo "Instantiating chaincode on peer1.pnu..."
instantiateChaincode 1 pnu

# Instantiate chaincode on peer1.pnu
# echo "Test chaincode on peer1.pnu..."
# testChaincode 1 pnu

# # Instantiate chaincode on peer1.pnu
# echo "Instantiating chaincode on peer1.pnu..."
# instantiateChaincode 0 2
