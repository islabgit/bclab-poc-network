package erc20

import (
	"encoding/json"
	"fmt"
	r "github.com/s7techlab/cckit/router"

	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"log"
	"math/big"
	"net"
	"strings"
	"time"
)

// var (
// 	host       = flag.String("host", "", "Comma-separated hostnames and IPs to generate a certificate for")
// 	validFrom  = flag.String("start-date", "", "Creation date formatted as Nov 1 15:04:05 2019")
// 	validFor   = flag.Duration("duration", 365*24*time.Hour, "Duration that certificate is valid for")
// 	isCA       = flag.Bool("ca", false, "whether this cert should be its own Certificate Authority")
// 	rsaBits    = flag.Int("rsa-bits", 2048, "Size of RSA key to generate. Ignored if --ecdsa-curve is set")
// 	ecdsaCurve = flag.String("ecdsa-curve", "", "ECDSA curve to use to generate a key. Valid values are P224, P256 (recommended), P384, P521")
// 	ed25519Key = flag.Bool("ed25519", false, "Generate an Ed25519 key")
// )

type (
	Device struct {
		DocType    string `json:"docType"` //docType is used to distinguish the various types of objects in state database
		DeviceName string `json:"deviceName"`
		PublicKey  string `json:"publicKey"` //TODO: Extend like DID Documents 
	}
	CertificateRequest struct {
		host string
		validFrom string
		validFor time.Duration
		isCA bool
		publicKey interface{}
		signerPrivateKey interface{}
	}
)

var (
	certPEMData = []byte(`
-----BEGIN CERTIFICATE-----
MIICKTCCAc+gAwIBAgIQG8+Yy3NYT5mADh/ZlV7OtTAKBggqhkjOPQQDAjBzMQsw
CQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEWMBQGA1UEBxMNU2FuIEZy
YW5jaXNjbzEZMBcGA1UEChMQZW5pdHQuZW5lcmd5Lm9yZzEcMBoGA1UEAxMTY2Eu
ZW5pdHQuZW5lcmd5Lm9yZzAeFw0xOTExMDcwODM1MDBaFw0yOTExMDQwODM1MDBa
MGsxCzAJBgNVBAYTAlVTMRMwEQYDVQQIEwpDYWxpZm9ybmlhMRYwFAYDVQQHEw1T
YW4gRnJhbmNpc2NvMQ4wDAYDVQQLEwVhZG1pbjEfMB0GA1UEAwwWQWRtaW5AZW5p
dHQuZW5lcmd5Lm9yZzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABKKp7rifVFCZ
ukZ2kPpwaxPfC6y9drru0szLq1YVZHjthHh0d7LaVsjAoWCpddFqlO3eD1qZpGOC
/CrUIFL+p7ujTTBLMA4GA1UdDwEB/wQEAwIHgDAMBgNVHRMBAf8EAjAAMCsGA1Ud
IwQkMCKAINksZDcGWohxKskHigAa5yk4oN/UFlWPdwF4suEKeWO6MAoGCCqGSM49
BAMCA0gAMEUCIQDyfOmCF+aoJqT+L5nJUyWuTQNGeIjgz7n3uQ/LnmGpVAIgPX4Z
Z9tcoJJfL+2LTIAWH3t735oqgVcpfIS99OjN2Fs=
-----END CERTIFICATE-----
`)

	privPEMData = []byte(`
-----BEGIN PRIVATE KEY-----
MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgT304Y9Z6ua5L1ohU
8fYsJf4ZWzcgJlfbaK6dwUiD2OOhRANCAASpGpLQ9dq4UpkcN2xmmH3+450ZTACu
38eFoV2F9oLgPFn4PUy9g7mP0KmROvwotmA89Q1rlelN2P4o8WDsLpCa
-----END PRIVATE KEY-----
`)
)

func getSignerPEMCert() string {
	return string(certPEMData)
}
func getPublicKey(priv interface{}) interface{} {
	switch k := priv.(type) {
	case *rsa.PrivateKey:
		return &k.PublicKey
	case *ecdsa.PrivateKey:
		return &k.PublicKey
	default:
		return nil
	}
}


func GenerateKeyPair(ecdsaCurve string) (publicKey, privateKey interface{}, err error) {

	
	switch ecdsaCurve {
	case "":
		privateKey, err = rsa.GenerateKey(rand.Reader, 2048)
	case "P224":
		privateKey, err = ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	case "P256":
		privateKey, err = ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	case "P384":
		privateKey, err = ecdsa.GenerateKey(elliptic.P384(), rand.Reader)
	case "P521":
		privateKey, err = ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	default:
		log.Fatalf("Unrecognized elliptic curve: %q", ecdsaCurve)
	}
	if err != nil {
		log.Fatalf("Failed to generate private key: %s", err)
	}
	publicKey = getPublicKey(privateKey)

	return
}

func GenerateCertificate(cr *CertificateRequest) (encodedCert []byte, err error) {

	var notBefore time.Time
	if len(cr.validFrom) == 0 {
		notBefore = time.Now()
	} else {
		notBefore, err = time.Parse("Jan 2 15:04:05 2019", cr.validFrom)
		if err != nil {
			log.Fatalf("Failed to parse creation date: %s", err)
		}
	}

	notAfter := notBefore.Add(cr.validFor)

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		log.Fatalf("Failed to generate serial number: %s", err)
	}

	template := x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"Enitt Co"},
		},
		NotBefore: notBefore,
		NotAfter:  notAfter,

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}

	hosts := strings.Split(cr.host, ",")
	for _, h := range hosts {
		if ip := net.ParseIP(h); ip != nil {
			template.IPAddresses = append(template.IPAddresses, ip)
		} else {
			template.DNSNames = append(template.DNSNames, h)
		}
	}

	if cr.isCA {
		template.IsCA = true
		template.KeyUsage |= x509.KeyUsageCertSign
	}

	cert, err := x509.CreateCertificate(rand.Reader, &template, &template, getPublicKey(cr.signerPrivateKey), cr.signerPrivateKey)
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	// certOut, err := os.Create("cert.pem")
	if err != nil {
		log.Fatalf("Failed to open cert.pem for writing: %s", err)
	}
	if encodedCert = pem.EncodeToMemory(&pem.Block{Type: "CERTIFICATE", Bytes: cert}); err != nil {
		log.Fatalf("Failed to write data to cert.pem: %s", err)
	}

	return

}

func invokeDeviceRegister(c r.Context) (interface{}, error) {
	docType := "device"
	deviceId := c.ParamString(`deviceId`)
	deviceName := c.ParamString(`deviceName`)
	// signedPublicKey := c.ParamString(`publicKey`)

	// ==== Check if deivce already exists ====
	// deviceAsBytes, err := c.Stub().GetState(deviceId)
	// if err != nil {
	// 	return nil, err
	// } else if deviceAsBytes != nil {
	// 	log.Fatalf("This device already exists: "+ deviceId)
	// 	return nil, err
	// }


	// ==== Set Certificate Request
	host := "127.0.0.1"
	validFrom := ""
	validFor := 365*24*time.Hour
	isCA := false

	block, _ := pem.Decode(privPEMData)
	if block == nil || block.Type != "PRIVATE KEY" {
		log.Fatal("failed to decode PEM block containing private key")
	}
	signerPrivateKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)
	if err != nil {
		log.Fatal("failed to parse DER encoded private key: " + err.Error())
	}


	devicePublicKey, devicePrivateKey, err := GenerateKeyPair("P256") 
	if err != nil {
		log.Fatalf("Failed to create certificate: %s", err)
	}

	cr := &CertificateRequest{host, validFrom, validFor, isCA, devicePublicKey, signerPrivateKey}

	encodedCert, err := GenerateCertificate(cr)
	// Encode Private Key
	marshaledPrivateKey, err := x509.MarshalPKCS8PrivateKey(devicePrivateKey)
	encodedPrivateKey := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: marshaledPrivateKey}); 
	if encodedPrivateKey == nil {
		log.Fatalf("Failed to encode privateKey pem: %s", err)
	}
	marshaledPublicKey, err := x509.MarshalPKIXPublicKey(devicePublicKey)
	encodedPublicKey := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: marshaledPublicKey}); 
	if encodedPublicKey == nil {
		log.Fatalf("Failed to encode publicKey pem: %s", err)
	}
	
	// Encode Public Key
	

	// ==== Create device object and marshal to JSON ====
	device := &Device{docType, deviceName, string(encodedPublicKey)}
	deviceJSONasBytes, err := json.Marshal(device)
	if err != nil {
		return nil, err
	}


	fmt.Println("\nCert :\n" + string(encodedCert))
	fmt.Println("Public Key :\n" + string(encodedPublicKey))
	fmt.Println("Private Key :\n" + string(encodedPrivateKey))

	response := fmt.Sprintf("{\"certificate\":\"%s\", \"privateKey\":\"%s\"}", string(encodedCert), string(encodedPrivateKey))

	// === Save device to state ===
	err = c.Stub().PutState(deviceId, deviceJSONasBytes)
	if err != nil {
		return nil, err
	}

	return response, nil

}

func queryDeviceById(c r.Context) (interface{}, error) {

	docType := "device"
	deviceId := c.ParamString(`deviceId`)

	queryString := fmt.Sprintf("{\"selector\":{\"docType\":\"%s\", \"_id\":\"%s\"}}", docType, deviceId)

	queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
	if err != nil {
		return nil, err
	}

	return queryResults, nil
}

