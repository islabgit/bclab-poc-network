package erc20

import (
	"encoding/json"
	"fmt"
	"io"
	"crypto/rand"

	"cityhub.org/token/zksnark"
	snark "github.com/arnaucube/go-snark"
	"github.com/pkg/errors"
	"github.com/s7techlab/cckit/identity"
	r "github.com/s7techlab/cckit/router"
)

const (
	BalancePrefix   = `BALANCE`
	AllowancePrefix = `APPROVE`
)

var (
	ErrNotEnoughFunds                   = errors.New(`not enough funds`)
	ErrForbiddenToTransferToSameAccount = errors.New(`forbidden to transfer to same account`)
	ErrSpenderNotHaveAllowance          = errors.New(`spender not have allowance for amount`)
)

type (
	Transfer struct {
		From   identity.Id
		To     identity.Id
		Amount int
	}

	Approve struct {
		From    identity.Id
		Spender identity.Id
		Amount  int
	}

	GenerateProofResult struct {
		ClaimId string      `json="claimId"`
		Proof   snark.Proof `json="proof"`
	}

	ClaimResult struct {
		VerifierId   string      `json="verifierId"`
		PersonalInfo string      `json="personalInfo"`
		Proof        snark.Proof `json="proof"`
		Setup        snark.Setup `json="setup"`
	}

)

func queryClaimList(c r.Context) (interface{}, error) {

	verifierId := c.ParamString(`verifierId`)

	queryString := fmt.Sprintf("{\"selector\":{\"VerifierId\":\"%s\"}}", verifierId)

	queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
	if err != nil {
		return nil, err
	}

	return queryResults, nil
}

func verifyProof(c r.Context) (interface{}, error) {
	claimId := c.ParamString(`claimId`)
	// publicInput := c.ParamString(`publicInput`)

	queryString := fmt.Sprintf("{\"selector\":{\"_id\":\"%s\"}}", claimId)

	_, err := getQueryResultForQueryString(c.Stub(), queryString)
	if err != nil {
		return nil, err
	}

	// queryResults, err := getQueryResultForQueryString(c.Stub(), queryString)
	// if err != nil {
	// 	return nil, err
	// }
	// claimResult := ClaimResult{}
	// json.Unmarshal([]byte(queryResults.(string)), &claimResult)

	// var resultMap map[string]interface{}
	// json.Unmarshal([]byte(queryResults), &resultMap)
	// fmt.Println(resultMap["record"])
	
	// verificationResult, _ := zksnark.VerifyProof(claimResult.Setup.Vk, claimResult.Proof, publicInput)

	// fmt.Println("Verification Result: ", verificationResult)
	fmt.Println("Verification Result: ", true)


	return true, nil
}

func generateProof(c r.Context) (interface{}, error) {

	verifierId := c.ParamString(`verifierId`)
	personalInfo := c.ParamString(`personalInfo`)
	privateInput := c.ParamString(`privateInput`)
	publicInput := c.ParamString(`publicInput`)

	fmt.Println("perfonsal info : ", personalInfo)

	// === Save claim info to state ===
	proof, setup := zksnark.GenerateProof(personalInfo, privateInput, publicInput)

	// proofAsBytes := Proof{Proof: proof}
	claimResult := ClaimResult{VerifierId: verifierId, PersonalInfo: personalInfo, Proof: proof.(snark.Proof), Setup: setup.(snark.Setup)}

	claimResultAsBytes, _ := json.Marshal(claimResult)

	uuid, err := newUUID()
	if err != nil {
		fmt.Printf("error: %v\n", err)
	}
	fmt.Printf("claimId: %s\n", uuid)

	err = c.Stub().PutState(uuid, claimResultAsBytes)
	if err != nil {
		return nil, err
	}

	result := GenerateProofResult{ClaimId: uuid, Proof: proof.(snark.Proof)}
	resultAsBytes, _ := json.Marshal(result)

	return resultAsBytes, nil
}

func trustedSetup(c r.Context) (interface{}, error) {
	// result, _ := zksnark.TrustedSetup("3", "35", "true")
	// fmt.Println("claim:", result)

	return c.State().Get(SymbolKey)
}

func newUUID() (string, error) {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return "", err
	}
	// variant bits; see section 4.1.1
	uuid[8] = uuid[8]&^0xc0 | 0x80
	// version 4 (pseudo-random); see section 4.1.3
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:]), nil
}

func querySymbol(c r.Context) (interface{}, error) {
	return c.State().Get(SymbolKey)
}

func queryName(c r.Context) (interface{}, error) {
	return c.State().Get(NameKey)
}

func queryTotalSupply(c r.Context) (interface{}, error) {
	return c.State().Get(TotalSupplyKey)
}

func queryBalanceOf(c r.Context) (interface{}, error) {
	return getBalance(c, c.ArgString(`mspId`), c.ArgString(`id`))
}

func invokeTransfer(c r.Context) (interface{}, error) {
	// transfer target
	fromMspId := c.ParamString(`fromMspId`)
	fromId := c.ParamString(`fromId`)
	toMspId := c.ParamString(`toMspId`)
	toId := c.ParamString(`toId`)

	//transfer amount
	amount := c.ParamInt(`amount`)

	// get information about tx creator
	// invoker, err := identity.FromStub(c.Stub())
	// if err != nil {
	// 	return nil, err
	// }

	// Disallow to transfer token to same account
	if fromMspId == toMspId && fromId == toId {
		return nil, ErrForbiddenToTransferToSameAccount
	}

	// get information about invoker balance from state
	invokerBalance, err := getBalance(c, fromMspId, fromId)
	if err != nil {
		return nil, err
	}

	// fmt.Printf("\n ================= Invoke Transfer =================\n")
	// fmt.Printf("To MSPID : %s \n", toMspId)
	// fmt.Printf("To ID : %s \n", toId)
	// fmt.Printf("Invoker MSPID : %s \n", fromMspId)
	// fmt.Printf("Invoker ID : %s \n", fromId)
	// fmt.Printf("Invoker Subject : %s \nInvoker PEM : %s \n", invoker.GetSubject(), invoker.GetPEM())
	// fmt.Printf("Invoker Balance : %d \n", invokerBalance)
	// fmt.Printf("invokerBalance-amount : %d \n", invokerBalance-amount)

	// fmt.Printf("\n =====================================================\n")

	// Check the funds sufficiency
	if invokerBalance-amount < 0 {
		return nil, ErrNotEnoughFunds
	}

	// Get information about recipient balance from state
	recipientBalance, err := getBalance(c, toMspId, toId)
	if err != nil {
		return nil, err
	}

	// Update payer and recipient balance
	if err = setBalance(c, fromMspId, fromId, invokerBalance-amount); err != nil {
		return nil, err
	}

	if err = setBalance(c, toMspId, toId, recipientBalance+amount); err != nil {
		return nil, err
	}

	// Trigger event with name "transfer" and payload - serialized to json Transfer structure
	if err = c.SetEvent(`transfer`, &Transfer{
		From: identity.Id{
			MSP:  fromMspId,
			Cert: fromId,
		},
		To: identity.Id{
			MSP:  toMspId,
			Cert: toId,
		},
		Amount: amount,
	}); err != nil {
		return nil, err
	}

	// return current invoker balance
	return invokerBalance - amount, nil
}

func queryAllowance(c r.Context) (interface{}, error) {
	return getAllowance(c, c.ParamString(`ownerMspId`), c.ParamString(`ownerId`), c.ParamString(`spenderMspId`), c.ParamString(`spenderId`))
}

func invokeApprove(c r.Context) (interface{}, error) {
	ownerMspId := c.ParamString(`ownerMspId`)
	ownerId := c.ParamString(`ownerId`)
	spenderMspId := c.ParamString(`spenderMspId`)
	spenderId := c.ParamString(`spenderId`)
	amount := c.ParamInt(`amount`)

	_, err := identity.FromStub(c.Stub())
	if err != nil {
		return nil, err
	}

	if err = setAllowance(c, ownerMspId, ownerId, spenderMspId, spenderId, amount); err != nil {
		return nil, err
	}

	if err = c.SetEvent(`approve`, &Approve{
		From: identity.Id{
			MSP:  ownerMspId,
			Cert: ownerId,
		},
		Spender: identity.Id{
			MSP:  spenderMspId,
			Cert: spenderId,
		},
		Amount: amount,
	}); err != nil {
		return nil, err
	}

	return true, nil
}

func invokeTransferFrom(c r.Context) (interface{}, error) {

	invokerMspId := c.ParamString(`invokerMspId`)
	invokerId := c.ParamString(`invokerId`)
	fromMspId := c.ParamString(`fromMspId`)
	fromId := c.ParamString(`fromId`)
	toMspId := c.ParamString(`toMspId`)
	toId := c.ParamString(`toId`)
	amount := c.ParamInt(`amount`)

	_, err := identity.FromStub(c.Stub())
	if err != nil {
		return nil, err
	}

	// check method invoker has allowances
	allowance, err := getAllowance(c, fromMspId, fromId, invokerMspId, invokerId)
	if err != nil {
		return nil, err
	}

	// transfer amount must be less or equal allowance
	if allowance < amount {
		return nil, ErrSpenderNotHaveAllowance
	}

	// current payer balance
	balance, err := getBalance(c, fromMspId, fromId)
	if err != nil {
		return nil, err
	}

	// payer balance must be greater or equal amount
	if balance-amount < 0 {
		return nil, ErrNotEnoughFunds
	}

	// current recipient balance
	recipientBalance, err := getBalance(c, toMspId, toId)
	if err != nil {
		return nil, err
	}

	// decrease payer balance
	if err = setBalance(c, fromMspId, fromId, balance-amount); err != nil {
		return nil, err
	}

	// increase recipient balance
	if err = setBalance(c, toMspId, toId, recipientBalance+amount); err != nil {
		return nil, err
	}

	// decrease invoker allowance
	if err = setAllowance(c, fromMspId, fromId, invokerMspId, invokerId, allowance-amount); err != nil {
		return nil, err
	}

	if err = c.Event().Set(`transfer`, &Transfer{
		From: identity.Id{
			MSP:  fromMspId,
			Cert: fromId,
		},
		To: identity.Id{
			MSP:  toMspId,
			Cert: toId,
		},
		Amount: amount,
	}); err != nil {
		return nil, err
	}

	// return current invoker balance
	return balance - amount, nil
}

// === internal functions, not "public" chaincode functions

// setBalance puts balance value to state
func balanceKey(mspId, userId string) []string {
	// fmt.Printf("Balance key : %s \n", []string{BalancePrefix, mspId, userId})
	return []string{BalancePrefix, mspId, userId}
}

func allowanceKey(ownerMspId, ownerId, spenderMspId, spenderId string) []string {
	// fmt.Printf("Allowance key : %s \n", []string{AllowancePrefix, ownerMspId, ownerId, spenderMspId, spenderId})
	return []string{AllowancePrefix, ownerMspId, ownerId, spenderMspId, spenderId}
}

func getBalance(c r.Context, mspId, userId string) (int, error) {
	return c.State().GetInt(balanceKey(mspId, userId), 0)
}

// setBalance puts balance value to state
func setBalance(c r.Context, mspId, userId string, balance int) error {
	return c.State().Put(balanceKey(mspId, userId), balance)
}

func getAllowance(c r.Context, ownerMspId, ownerId, spenderMspId, spenderId string) (int, error) {
	return c.State().GetInt(allowanceKey(ownerMspId, ownerId, spenderMspId, spenderId), 0)
}

func setAllowance(c r.Context, ownerMspId, ownerId, spenderMspId, spenderId string, amount int) error {
	return c.State().Put(allowanceKey(ownerMspId, ownerId, spenderMspId, spenderId), amount)
}
