package erc20

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/s7techlab/cckit/extensions/owner"
	"github.com/s7techlab/cckit/router"
	p "github.com/s7techlab/cckit/router/param"
)

const SymbolKey = `symbol`
const NameKey = `name`
const TotalSupplyKey = `totalSupply`
const OwnerMSP = `PNUMSP`
const OwnerID = `admin`


func NewErc20FixedSupply() *router.Chaincode {
	fmt.Printf("Init Power Related Chaincode \n")
	r := router.New(`erc20fixedSupply`).Use(p.StrictKnown).

		// Chaincode init function, initiates token smart contract with token symbol, name and totalSupply
		Init(invokeInitFixedSupply, p.String(`symbol`), p.String(`name`), p.Int(`totalSupply`)).

		// Get token symbol
		Query(`symbol`, querySymbol).

		// Get token name
		Query(`name`, queryName).

		// Get the total token supply
		Query(`totalSupply`, queryTotalSupply).

		//  get account balance
		Query(`balanceOf`, queryBalanceOf, p.String(`mspId`), p.String(`id`)).

		//Send value amount of tokens
		Invoke(`transfer`, invokeTransfer, p.String(`fromMspId`), p.String(`fromId`), p.String(`toMspId`), p.String(`toId`), p.Int(`amount`)).

		// Allow spender to withdraw from your account, multiple times, up to the _value amount.
		// If this function is called again it overwrites the current allowance with _valu
		Invoke(`approve`, invokeApprove, p.String(`ownerMspId`), p.String(`ownerId`), p.String(`spenderMspId`), p.String(`spenderId`), p.Int(`amount`)).

		//    Returns the amount which _spender is still allowed to withdraw from _owner]
		Query(`allowance`, queryAllowance, p.String(`ownerMspId`), p.String(`ownerId`),
			p.String(`spenderMspId`), p.String(`spenderId`)).

		// Send amount of tokens from owner account to another
		Invoke(`transferFrom`, invokeTransferFrom, p.String(`invokerMspId`), p.String(`invokerId`), p.String(`fromMspId`), p.String(`fromId`),
			p.String(`toMspId`), p.String(`toId`), p.Int(`amount`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Invoke(`trade`, invokeTrade, p.String(`tradeId`), p.String(`buyerId`), p.String(`sellerId`), p.String(`power`), p.String(`time`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`tradeByUser`, queryTradeByUser, p.String(`userId`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`tradeByTime`, queryTradeByTime, p.String(`termStart`), p.String(`termEnd`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Invoke(`transmissionPower`, invokeTransmissionPowerHistory, p.String(`transmissionId`), p.String(`femsId`), p.String(`termStart`), p.String(`termEnd`), p.String(`power`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`transmissionPowerByFEMS`, queryTransmissionPowerHistory, p.String(`femsId`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Invoke(`receivePower`, invokeReceivePowerHistory, p.String(`receiveId`), p.String(`femsId`), p.String(`termStart`), p.String(`termEnd`), p.String(`power`)).

		// TODO: Refactoring: Separate erc20 / power interface
		Query(`receivePowerByFEMS`, queryReceivePowerHistory, p.String(`femsId`)).

		// TODO: Refactoring: Separate erc20 / device interface
		Invoke(`deviceRegister`, invokeDeviceRegister, p.String(`deviceId`), p.String(`deviceName`), p.String(`publicKey`)).

		// TODO: Refactoring: Separate erc20 / device interface
		Query(`deviceById`, queryDeviceById, p.String(`deviceId`)).

		// Trusted Setup
		// Invoke(`trustedSetup`, trustedSetup, p.String(`privateInput`), p.String(`publicInput`), p.String(`claim`)).

		// Generate Proof
		Invoke(`generateProof`, generateProof, p.String(`verifierId`), p.String(`personalInfo`), p.String(`privateInput`), p.String(`publicInput`)).

		// Verify Proof
		Invoke(`verifyProof`, verifyProof, p.String(`claimId`), p.String(`publicInput`)).

		// Get Claim List by verifier ID
		Query(`claimlist`, queryClaimList, p.String(`verifierId`))
		


	return router.NewChaincode(r)
}

func invokeInitFixedSupply(c router.Context) (interface{}, error) {

	ownerIdentity, err := owner.SetFromCreator(c)
	if err != nil {
		return nil, errors.Wrap(err, `set chaincode owner`)
	}

	// save token configuration in state
	if err := c.State().Insert(SymbolKey, c.ParamString(`symbol`)); err != nil {
		fmt.Printf("token symbol already exist")
		// return nil, err
	}

	if err := c.State().Insert(NameKey, c.ParamString(`name`)); err != nil {
		fmt.Printf("token name already exist")
		// return nil, err
	}

	if err := c.State().Insert(TotalSupplyKey, c.ParamInt(`totalSupply`)); err != nil {
		fmt.Printf("token totalSupply already exist")
		// return nil, err
	}

	// fmt.Printf("Owner MSP : %s, Owner ID : %s \n", ownerIdentity.GetMSPID(), ownerIdentity.GetID())

	// set token owner initial balance
	fmt.Printf("\n================= Init Token System =================\n")
	fmt.Printf("Token symbol : %s \n", c.ParamString(`symbol`))
	fmt.Printf("Token name : %s \n", c.ParamString(`name`))
	fmt.Printf("Token total supply : %d \n", c.ParamInt(`totalSupply`))
	fmt.Printf("Owner MSPID : %s \n", OwnerMSP)
	fmt.Printf("Owner ID : %s \n", OwnerID)
	fmt.Printf("Owner Subject : %s \nOwner PEM : \n%s", ownerIdentity.GetSubject(), ownerIdentity.GetPEM())
	fmt.Printf("=====================================================\n")

	if err := setBalance(c, OwnerMSP, OwnerID, c.ParamInt(`totalSupply`)); err != nil {
		return nil, errors.Wrap(err, `set owner initial balance`)
	}

	return ownerIdentity, nil
}
