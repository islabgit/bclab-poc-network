module cityhub.org/token

go 1.13

// replace (
// 	github.com/pkg/errors v0.8.1 => ./vendor/github.com/pkg/errors
// 	github.com/s7techlab/cckit v0.6.3 => ./vendor/github.com/s7techlab/cckit
// 	github.com/sykesm/zap-logfmt v0.0.2 => ./vendor/github.com/sykesm/zap-logfmt
// )

require (
	github.com/arnaucube/go-snark v0.0.4
	github.com/hyperledger/fabric v1.4.3
	github.com/hyperledger/fabric-lib-go v1.0.0 // indirect
	github.com/pkg/errors v0.8.1
	github.com/s7techlab/cckit v0.6.3
	github.com/sykesm/zap-logfmt v0.0.2 // indirect
)
