package zksnark

import (
	"bytes"
	// "fmt"
	"math/big"
	"reflect"
	"strconv"
	"strings"

	snark "github.com/arnaucube/go-snark"
	"github.com/arnaucube/go-snark/circuitcompiler"

	// cc "github.com/arnaucube/go-snark/circuitcompiler"
	"github.com/arnaucube/go-snark/r1csqap"
)

type Claim struct {
	ID           string   `json="ID"`
	Vk           snark.Vk `json="Vk"`
	Pk           snark.Pk `json="Pk"`
	PersonalInfo string   `json="PersonalInfo"`
}

var code = `
func main(private s0, public s1):
	s2 = s0 * s0
	s3 = s2 * s0
	s4 = s3 + s0
	s5 = s4 + 5
	equals(s1, s5)
	out = 1 * 1
`

func GenerateProof(personalInfo string, privateInput string, publicInput string) (interface{}, interface{}) {
	// parse the code
	parser := circuitcompiler.NewParser(strings.NewReader(code))
	circuit, err := parser.Parse()

	privateInputs, err := parseBigInt64Array(privateInput)
	if err != nil {
		return nil, err
	}

	publicSignals, err := parseBigInt64Array(publicInput)
	// witness
	w, err := circuit.CalculateWitness(privateInputs, publicSignals)

	// code to R1CS
	a, b, c := circuit.GenerateR1CS()

	// R1CS to QAP
	// TODO zxQAP is not used and is an old impl, TODO remove
	alphas, betas, gammas, _ := snark.Utils.PF.R1CSToQAP(a, b, c)
	if len(alphas) != 8 || len(betas) != 8 || len(gammas) != 8 {
		return nil, err
	}

	ax, bx, cx, px := snark.Utils.PF.CombinePolynomials(w, alphas, betas, gammas)
	if len(ax) != 7 || len(bx) != 7 || len(cx) != 7 || len(px) != 13 {
		return nil, err
	}

	// calculate trusted setup
	setup, err := snark.GenerateTrustedSetup(len(w), *circuit, alphas, betas, gammas)
	if err != nil {
		return nil, err
	}
	hx := snark.Utils.PF.DivisorPolynomial(px, setup.Pk.Z)
	div, rem := snark.Utils.PF.Div(px, setup.Pk.Z)
	if !objectsAreEqual(hx, div) {
		return nil, err
	}
	if !objectsAreEqual(rem, r1csqap.ArrayOfBigZeros(6)) {
		return nil, err
	}

	// hx==px/zx so px==hx*zx
	if !objectsAreEqual(px, snark.Utils.PF.Mul(hx, setup.Pk.Z)) {
		return nil, err
	}

	// check length of polynomials H(x) and Z(x)
	if len(hx) != len(px)-len(setup.Pk.Z)+1 {
		return nil, err
	}

	// claimObject := Claim{ID: "uniqueId0", Pk: setup.Pk, Vk: setup.Vk, PersonalInfo: personalInfo}
	//TODO: PutState(jsonClaim)
	// jsonClaim, _ := json.Marshal(claimObject)
	// zk.stub.PutState("uniqueId0", jsonClaim)

	// out := Output{circuit: circuit, witness: w, px: px, publicSignals: publicSignals}
	// jsonOut, _ := json.Marshal(out)

	proof, err := snark.GenerateProofs(*circuit, setup.Pk, w, px) // circuit, provingKey, witness, px
	if err != nil {
		return nil, err
	}

	// proofOut := Proof{Proof: proof}
	// jsonProof, _ := json.Marshal(proofOut)

	// json.Unmarshal(jsonProof, &proofOut)
	// fmt.Println("Proof:", jsonProof)

	return proof, setup

}

func VerifyProof(vk snark.Vk, proof snark.Proof, publicInput string) (interface{}, error) {

	// 0 : proof, 1 : publicSignals, 2 : ID
	publicSignalsVerif, err := parseBigInt64Array(publicInput) // publicSignals
	if err != nil {
		return nil, err
	}

	// proof := snark.Proof{}

	// claim := Claim{}
	// jsonClaim, _ := zk.stub.GetState(claimId)
	// json.Unmarshal(jsonClaim, &claim)
	if !snark.VerifyProof(vk, proof, publicSignalsVerif, true) {
		return false, err
	}

	return true, err
}

func parseBigInt64Array(input string) ([]*big.Int, error) {
	publicInput, err := strconv.ParseInt(input, 10, 64)
	if err != nil {
		return nil, err
	}
	output := []*big.Int{big.NewInt(publicInput)}
	return output, nil
}

func objectsAreEqual(expected, actual interface{}) bool {
	if expected == nil || actual == nil {
		return expected == actual
	}

	exp, ok := expected.([]byte)
	if !ok {
		return reflect.DeepEqual(expected, actual)
	}

	act, ok := actual.([]byte)
	if !ok {
		return false
	}
	if exp == nil || act == nil {
		return exp == nil && act == nil
	}
	return bytes.Equal(exp, act)
}
